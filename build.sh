#!/bin/bash

mkdir -p log
DATE=`date +%Y%m%d-%H%M%S`
bash exec-build.sh 2>&1 | tee log/log-$DATE.txt
