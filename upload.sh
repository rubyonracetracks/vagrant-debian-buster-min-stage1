#!/bin/bash

set -e

source variables.sh

VERSION=`date +%Y%m%d-%H%M`

echo '###############################################################################################'
echo "vagrant cloud publish --force $OWNER/$BOX_NAME $VERSION $VERSION virtualbox $BOX_FILE --release"
echo 'NOTE: This is a LONG process!'
vagrant cloud publish --force $OWNER/$BOX_NAME $VERSION $VERSION virtualbox $BOX_FILE --release
