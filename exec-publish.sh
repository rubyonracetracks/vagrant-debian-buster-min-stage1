#!/bin/bash

set -e

bash check-login.sh

bash exec-build.sh

bash upload.sh
